# opensuse-notebook

vmdk installation with 12G 

for a 14.4.G pendrive target, with vmdk => raw, and also for notebook.

it allows to run 15.3 leap desktop, with XFCE4

## Recommended - Stable, tested, and default


````
  wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/opensuse-notebook/-/raw/main/pub/v0/harddisk.img.gz"
  
````

Link: https://gitlab.com/openbsd98324/opensuse-notebook/-/raw/main/pub/v0/harddisk.img.gz



## Version 0 , clean from vbox with vmdk, converted to img  (Stable)

https://gitlab.com/openbsd98324/opensuse-notebook/-/raw/main/pub/v0/harddisk.img.gz





## Version 1, updated with newer kernel chimaera  (testing)

And xfce4, and bootable to (gpt2) and mmcblk0p2 !

Kernel, lib, with modified grub.cfg (mmc and sdb2)

https://gitlab.com/openbsd98324/opensuse-notebook/-/raw/main/pub/v1/kernel-pc-dell-v1.2.tar.gz
